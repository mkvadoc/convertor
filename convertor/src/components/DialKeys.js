import DialButton from "./DialButton";
import "./DialKeys.css";
import { dialpad, } from "../data";
import { useContext } from "react";
import NumberContext from './NumbersContext';


const DialKeys = () => {
    const { onClickDeleteHandler } = useContext(NumberContext);

    return (
        <>
            <div className="dial-keyboard">
                {dialpad.map((oneDialPad, index) => {
                    const { number, letters } = oneDialPad;
                    return (
                        <DialButton
                            key={index}
                            number={number}
                            letters={letters}
                        />
                    );
                })}
                <button className="delete-button" onClick={onClickDeleteHandler}>X</button>
            </div>
        </>
    );
};

export default DialKeys;
