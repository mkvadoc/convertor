import { useContext } from 'react';
import './DialButton.css'
import NumberContext from './NumbersContext';

const DialButton = (props) => {
    const { onClickHandler } = useContext(NumberContext);

    return (
        <button className='one-dial-button' onClick={onClickHandler} value={props.number} >
            <h3>{props.number}</h3>
            <p>{props.letters}</p>
        </button>
    )
}

export default DialButton