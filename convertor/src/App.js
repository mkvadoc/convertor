import React, { useState } from "react";
import "./App.css"
import DialKeys from './components/DialKeys'
import DisplayBar from './components/DisplayBar'
import NumberContext from "./components/NumbersContext";
const App = () => {
  const [numberValue, setNumberValue] = useState([]);

  const onClickHandler = (e) => {
    const value = e.target.value;
    setNumberValue((prevValue) => {
      return [...prevValue, value];
    });
  };

  const onClickDeleteHandler = () => {
    const newArrayNumber = [...numberValue];
    newArrayNumber.pop();
    setNumberValue(newArrayNumber);
  };

  return (
    <NumberContext.Provider value={{ numberValue, onClickHandler, onClickDeleteHandler }}>
      <div className='app'>
        <DialKeys />
        <DisplayBar />
      </div>
    </NumberContext.Provider >
  )
}

export default App