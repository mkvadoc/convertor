import React, { useContext, useEffect, useState } from 'react'
import NumberContext from './NumbersContext';
import { words, digitsToLetters } from '../data';
import "./DisplayBar.css"

const DisplayBar = () => {
    const { numberValue } = useContext(NumberContext);
    const [filteredWords, setFilteredWords] = useState([]);

    const number = numberValue.join("")

    const letterCombinations = (digits) => {

        if (digits === '') {
            return [];
        }

        const possibleValues = digits.split('').map(value => digitsToLetters[value]);

        const possibleCombinations = [];

        const recursiveCombine = (start, result) => {

            if (result.length === digits.length) {
                possibleCombinations.push(result.join(''));
            }

            for (var i = start; i < possibleValues.length; i++) {
                for (var j = 0; j < possibleValues[i].length; j++) {
                    result.push(possibleValues[i][j]);
                    recursiveCombine(i + 1, result);
                    result.pop();
                }
            }
        }

        recursiveCombine(0, []);
        return possibleCombinations;
    };

    useEffect(() => {
        const combination = letterCombinations(number);

        const filtered = words.filter(word1 =>
            combination.some(word2 => word1.toLowerCase().includes(word2))
        );

        setFilteredWords(filtered);
    }, [number]);

    return (
        <>
            <h2>{numberValue}</h2>            <ul>
                {filteredWords.map((word, index) => (
                    <li key={index}>
                        <h4> {word} </h4>
                    </li>
                ))}
            </ul>
        </>
    )
}

export default DisplayBar